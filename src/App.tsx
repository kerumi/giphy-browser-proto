import { useState } from 'react';
import './App.css';
import { useMutation, gql, useQuery } from '@apollo/client';

const ADD_SEARCH_TERM = gql`
  mutation AddSearchTerm($term: String!) {
    addSearchTerm(term: $term)
  }
`;

const GET_HISTORY = gql`
  query GetHistory {
    history
  }
`;

const CLEAR_HISTORY = gql`
  mutation ClearHistory {
    clearHistory
  }
`;

interface GiphyResponse {
  data: Array<{
    id: string;
    images: {
      original: {
        url: string;
      };
      fixed_height_small: {
        url: string;
      };
    };
  }>;

  pagination: {
    total_count: number;
    count: number;
    offset: number;
  };
}

function App() {
  const [searchTerm, setSearchTerm] = useState('');
  const [gifResults, setGifResults] = useState<GiphyResponse['data']>([]);
  const [offset, setOffset] = useState(0);
  const [total, setTotal] = useState<number | null>(null);
  const { data, error, loading, refetch } = useQuery(GET_HISTORY);
  const [addSearchTerm] = useMutation(ADD_SEARCH_TERM, {
    onCompleted: refetch,
  });
  const [clearHistory] = useMutation(CLEAR_HISTORY, {
    onCompleted: refetch,
  });

  const fetchGif = async (term: string, offset: number = 0) => {
    const response = await fetch(
      `https://api.giphy.com/v1/gifs/search?api_key=${import.meta.env.VITE_GIPHY_API_KEY}&q=${term}&limit=10&offset=${offset}`
    );
    const result: GiphyResponse = await response.json();

    if (offset === 0) {
      setGifResults(result.data);
    } else {
      setGifResults((prev) => [...prev, ...result.data]);
    }

    setTotal(result.pagination.total_count);
    setOffset(offset + result.pagination.count);
  };

  const handleSearch = () => {
    addSearchTerm({ variables: { term: searchTerm } });
    fetchGif(searchTerm);
    // setSearchTerm('');
  };

  const handleClearHistory = () => {
    clearHistory();
  };

  const loadMoreResults = () => {
    fetchGif(searchTerm, offset);
  };

  return (
    <div className="App">
      <div>
        <h1>GIF Browser</h1>
        <div className="browser-controls">
          <header className="App-header">
            <input
              type="text"
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}
              placeholder="Search for GIF"
            />
            <button onClick={handleSearch}>Search</button>
          </header>
        </div>

        <div className="gif-gallery">
          {gifResults.map((gif) => (
            <img
              key={gif.id}
              src={gif.images.fixed_height_small.url}
              alt="GIF"
            />
          ))}
        </div>

        {total !== null && offset < total && (
          <button onClick={loadMoreResults}>Load More</button>
        )}
      </div>

      <div className="history-section fixed-bottom-section">
        <h2>Search History</h2>
        {loading && <p>Loading...</p>}
        {error && <p>Error: {error.message}</p>}
        {data && (
          <ul>
            {data.history.map((term: string, index: number) => (
              <li key={index}>{term}</li>
            ))}
          </ul>
        )}
        <button onClick={handleClearHistory}>Clear History</button>
      </div>
    </div>
  );
}

export default App;
