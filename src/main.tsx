import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import {
  ApolloProvider,
  ApolloClient,
  InMemoryCache,
  createHttpLink,
} from '@apollo/client';
import './index.css';

const link = createHttpLink({
  uri: 'http://localhost:9000/graphql',
});

const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
});

ReactDOM.createRoot(document.getElementById('root')!).render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);
