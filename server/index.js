const express = require('express');
const { ApolloServer } = require('apollo-server-express');

const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const typeDefs = require('./schema');
const resolvers = require('./resolvers');

const app = express();

const PORT = 9000;

app.use(bodyParser.json());
app.use(cors());

app.use(express.static(path.join(__dirname, '../build')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../build', 'index.html'));
});

(async () => {
  const server = new ApolloServer({ typeDefs, resolvers });

  await server.start();

  server.applyMiddleware({ app });

  app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
    console.log(`GraphiQL is available at http://localhost:${PORT}/graphql`);
  });
})();
