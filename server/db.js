const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database(':memory:', (err) => {
  if (err) {
    console.error(err.message);
  } else {
    db.run(
      `CREATE TABLE searchHistory (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            term TEXT
        )`,
      (err) => {
        if (err) {
          console.error(err.message);
        }
      }
    );
  }
});

module.exports = db;
