const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type Query {
    history: [String]
  }

  type Mutation {
    addSearchTerm(term: String!): String
    clearHistory: String
  }
`;

module.exports = typeDefs;
