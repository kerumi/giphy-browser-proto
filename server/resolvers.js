const db = require('./db');

const resolvers = {
  Query: {
    history: () => {
      return new Promise((res, rej) => {
        db.all('SELECT term FROM searchHistory', [], (err, rows) => {
          if (err) {
            rej(err);
          } else {
            res(rows.map((row) => row.term));
          }
        });
      });
    },
  },

  Mutation: {
    addSearchTerm: (_, { term }) => {
      return new Promise((res, rej) => {
        db.run('INSERT INTO searchHistory (term) VALUES (?)', [term], (err) => {
          if (err) {
            rej(err);
          } else {
            res(term);
          }
        });
      });
    },
    clearHistory: () => {
      return new Promise((res, rej) => {
        db.run('DELETE FROM searchHistory', [], (err) => {
          if (err) {
            rej(err);
          } else {
            res('History cleared');
          }
        });
      });
    },
  },
};

module.exports = resolvers;
